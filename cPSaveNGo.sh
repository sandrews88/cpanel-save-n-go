#!/bin/bash
# cPanel Save 'n Go v0.2-7.3.2015
# smandrews@softlayer.com/smichael@us.ibm.com
#
# Changelog -
# v0.1 - inception
# v0.2-7.3.2015 - Added sdb safety checking to rule out auto-mounting secondary drives
# v0.3-7.5.2015 - Added partition verification checks to ensure it's mounting the right partition to the right mount point
# v0.4-7.6.2015 - Added screen verfication
#
clear
echo -e "\e[1;31mYou MUST be in screen before running this script."
read -p "Are you in screen? Please respond with 'y' or 'Y' - "
echo -e "\n\n"
	if [[ $REPLY =~ [Yy]$ ]]
		then
			tput sgr0 
			echo "Great, we will continue on now."
		else
			exit 0
	fi
echo -e "\e[94m############################################################"
echo -e "\e[94m# Welcome to the \e[38;5;166mcPanel \e[94mSave 'n Go script.                 #"
echo -e "\e[94m# This is script is meant to be used to backup cPanel data #"
echo -e "\e[94m# on a failed drive and then restore it to the new one.    #"
echo -e "\e[94m# If you have any questions about this script, please      #"
echo -e "\e[94m# contact\e[38;5;226m smandrews@softlayer.com.                         \e[94m#"
echo -e "\e[94m############################################################"
echo -e "\n\n"
echo -e "\e[1;31mThis script is meant for use ONLY with cPanel servers that"
echo -e "\e[1;31mhave the default partition scheme (partition sizes do not matter)."
echo -e "\e[1;31mIf you have ANYTHING different, please CTRL+C now!"
tput sgr0
sleep 10
echo -e "\e[94mChecking partitions of failed drive to make sure they exist...\n"
tput sgr0

# Checking the drives to make sure the partitions show up
olddisk=$(fdisk -l | grep dev | egrep -v 'Disk|sda'| awk '{print $1}')

#####
# The following line is simply used for error checking during the script and needs to stay commented out
#olddisk=$(fdisk -l | grep dev | egrep -v 'sdb|sda')
#####

sleep 1

# Check to verify that a partition actually is showing.  If yes, continue on; otherwise fail and stop.
        if [[ $olddisk == *"/dev/sd"* ]]
                then
                        echo -e "\n\e[94mSo looks like \e[38;5;82m`echo $olddisk | awk '{print $1}'| rev | cut -c 2- | rev` \e[94mis showing and good to go."
                else
                        echo -e "\n\e[31mThe drive is not showing up, exiting now."
                        tput sgr0
                        exit 0
        fi

# Create the partitions to to prep for the mount
echo -e "\e[94mCreating folders to prepare the mount the partitions..."
sleep 1
olddir=$(mkdir -v /old /old/boot /old/home /old/usr /old/var /old/tmp)
sleep 1

#Safety check to make sure it's actually going to mount the correct device and not any secondary drives
#Will work on something for including /home2 /home3 etc later, for now just trying to get single /home backups working with this safety check
safemount=$(echo $olddisk | awk '{print $1}'| rev | cut -c 2- | rev)
        if [[ $safemount != *"/dev/sdb"* ]]
			then
				tput sgr0
                exit 0
        fi
########
#Mount point checks to verify the data is correct before mounting.
########
sleep 1
clear
mount `echo $safemount`6 /old
cd /old
oldslash=$(find -maxdepth 1 -type d -name root)
        if [[ $oldslash == *"root"* ]]
			then
                echo -e "\e[32mThis is a valid / partition.  Check passed, continuing..."
            else
                echo -e "\e[31mThis is not a valid / partition, exiting script."
                cd /
                umount /old
                tput sgr0
                exit 0
        fi
mount `echo $safemount`1 /old/boot
cd /old/boot
oldboot=$(find -iname "initrd*" | cut -d - -f 1 | uniq)
		if [[ $oldboot == *"initrd"* ]]
			then
                echo -e "\e[32mThis is a valid /boot parition.  Check passed, continuing..."
			else
                echo -e "\e[31mThis is not a valid /boot partition, exiting script."
                cd /
                umount /old/boot
                tput sgr0
                exit 0
        fi
mount `echo $safemount`8 /old/home
cd /old/home
oldhome=$(find -maxdepth 1 -type d -name cpeasyapache)
        if [[ $oldhome == *"cpeasyapache"* ]]
			then
                echo -e "\e[32mThis is a valid /home partition.  Check passed, continuing..."
        else
                echo -e "\e[31mThis is not a valid /home partition, exiting script."
                cd /
                umount /old/home
                tput sgr0
                exit 0
        fi
mount `echo $safemount`7 /old/tmp		
cd /old/tmp
oldtmp=$(find -type d -iname "*ICE-unix")
		if [[ $oldtmp == *"ICE"* ]]
			then
				echo -e "\e[32mThis is a valid /tmp partition.  Check passed, continuing..."
		else
			echo -e "\e[31mThis is not a valid /tmp partition, exiting script."
			cd /
			umount /old/tmp
			tput sgr0
			exit 0
		fi
mount `echo $safemount`2 /old/usr
cd /old/usr
oldusr=$(find -type d -iname sbin)
	if [[ $oldusr == *"sbin"* ]]
		then
			echo -e "\e[32mThis is a valid /usr partition.  Check passed, continuing..."
	else
		echo -e "\e[31mThis is not a valid /usr partition, exiting script."
		cd /
		umount /old/usr
		tput sgr0
		exit 0
	fi
mount `echo $safemount`3 /old/var
cd /old/var
oldvar=$(find -type f -iname messages)
	if [[ $oldvar == *"messages"* ]]
		then
			echo -e "\e[32mThis is a valid /usr partition.  Check passed, moving onto the next step..."
		else
			echo -e "\e[31mThis is not a valid /var partition, exiting script."
			cd /
			umount /old/var
			tput sgr0
			exit 0
	fi
	
############Commented out as this was part of v0.1 check
# Mount the partitions
#echo -e "\e[94mMounting partitions under the /old directory..."
#mount `echo $olddisk | awk '{print $1}'| rev | cut -c 2- | rev`6 /old
#mount `echo $olddisk | awk '{print $1}'| rev | cut -c 2- | rev`1 /old/boot
#mount `echo $olddisk | awk '{print $1}'| rev | cut -c 2- | rev`8 /old/home
#mount `echo $olddisk | awk '{print $1}'| rev | cut -c 2- | rev`7 /old/tmp
#mount `echo $olddisk | awk '{print $1}'| rev | cut -c 2- | rev`2 /old/usr
#mount `echo $olddisk | awk '{print $1}'| rev | cut -c 2- | rev`3 /old/var
#sleep 1
#echo -e "\e[94mAll partitions should be successfully mounted at this time."
############

# Prepping for chrooted environment
echo -e "\e[94mPreparing to chroot to the old environment..."
sleep 1
echo -e "\e[94mStopping MySQL..."
service mysql stop
echo -e "\n\e[1;31m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo -e "\e[1;31m!!!!!!!!! Copy down the following command as you will need it for the next part. !!!!!!!!!"
echo -e "\e[1,31m!!!!!!!!! This is not something I could script because I don't know how yet.     !!!!!!!!!"
echo -e "\n\e[1;31m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"
echo -e "\e[38;5;226mmount proc /proc -t proc; service mysql start; mknod /dev/null c 1 3 ; for i in \`cat /etc/trueuserdomains | awk '{print \$2}'\`; do scripts/pkgacct \$i ; done ; service mysql stop ; mount proc /proc -t proc ; killall -6 mysqld ; sleep 2 ; killall -6 mysqld ; sleep 2 ; exit"
tput sgr0
chroot /old

# Move all the cpmove data over to the new /home directory so that it can restore
echo -e "\n\e[94mAt this point, everything should be backed up; now we move it over.\n"
rsync -avh /old/home/cpmove-*.tar.gz /home
service mysql start

# Restore the content
for f in `ls /home/cpmove*` ; do /scripts/restorepkg --force $f ; done
clear
echo -e "\e[94mCongratulations!  Everything has been restored to the new server.  Now you can have the DC disconnect the old drive."
tput sgr0
cd /root
rm -fv cPSaveNGo.sh